# AWNA

## Description
**awna** stands for Automatic Wireless Network Access and it 
is an OpenBSD specific ksh script to manage wireless networks
it presents a list of available networks and allows the user to
connect them using something that is almost like a GUI in terms of 
ease of use.

A few commands used by
**awna**
require special permissions
so it should be run as root.

## Dependencies

Before using this script edit the name of the wireless interface
at the top of the script to suit your wlan interface.

If you want to install it just run cp awna /usr/local/bin/ as root

If you don't know how to find out which one of your interfaces is the wireless one
type ifconfig and look for groups under each interface, your wifi interface is the one 
that is in wlan group.

## Usage

It's preety easy to use. Edit the name of the wireless interface at the top to your interface's name.
Then just run awna as root and you get a list of networks.
Choose a network you are willing to connect to by typing a number corresponding to that network 
then enter the WPA key (will echo) choose whether you want DHCP or not and you are connected.

There is one thing to consider though. If your interface was initially disabled it might take 
some time to start up and see all the networks and while the script automatically starts it for 
you it doesn't wait for it to wake up, so if you don't see any networks where you should just
wait for 5-10 seconds and rerun the script

## Warning

Be forwarned that this script is REALLY simple and somewhat dumb, so if you terminate it in the 
middle of configuring a network it might break your connection and you will have to edit the 
hostname files to restore functionality  

## Contributing
Contributions are always welcome and ecouraged. 
